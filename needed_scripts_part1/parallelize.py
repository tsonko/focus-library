#!/usr/bin/python
import getopt
import sys
import MySQLdb as mdb
import math
from multiprocessing import Process, Queue

def splitInChunks(l, n):
    """
    Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def submitWorker(queue, lines, functionName, db):
    # import external module
    exec("import " + functionName)

    # init results list
    results = list()

    # connect to MySQL databese
    con = False
    if db:
        con = mdb.connect('localhost', 'chembl_22', 'chembl_22', 'chembl_22')
        #con = mdb.connect('localhost', 'chembl_20', 'chembl_20', 'chembl_20')
        #con = mdb.connect('localhost', 'chembl_19', 'chembl_19', 'chembl_19')

    # loop through the lines and submit db search
    for line in lines:
            exec("results = results + "+functionName+".processData(line[:-1].strip(), con)")
    queue.put(results)

    # close database connection
    if db:
        con.close()



def run_paralel(chunks, functionName, inputFileName, outputFileName, cpus, db=True):
    results = list()
    # run in parallel
    q = Queue()
    procs = list()
    for i, chunk in enumerate(chunks):
        p = Process(target=submitWorker, args=(q, chunk, functionName, db))
        procs.append(p)
        p.start()
    for chunk in chunks:
        results = results + q.get()

    for p in procs:
        p.join()

    
    # write output to file
    with open(outputFileName, "a") as outputFile:
        if len(results) != 0:
            outputFile.write("\n".join(results) + '\n')

def prepareInput(functionName, inputFileName, outputFileName, cpus, db=True):
    """
    Prepares input data, splits it into chunks and submits in parallel
    """
    # result list
    results = list()
    lines = []
    # read input file
    fh_input = open(inputFileName, 'r')

    for line in fh_input:

        if len(lines) < int(cpus)*10000-1:
            lines.append(line)
            
        elif len(lines)==int(cpus)*10000-1:
            lines.append(line)
            # split the dataset into equal pieces
            chunks = list(splitInChunks(lines, int(math.ceil(len(lines)/float(cpus)))))
            run_paralel(chunks, functionName, inputFileName, outputFileName, cpus, db=True)
            lines = []
    if len(lines)!=0:
        chunks = list(splitInChunks(lines, int(math.ceil(len(lines)/float(cpus)))))
        run_paralel(chunks, functionName, inputFileName, outputFileName, cpus, db=True)


