#!/usr/bin/env python
import sys
from rdkit import Chem
from collections import Counter

#the function tests for open aromatic rings
def open_aromatic_ring_test(smi, name):
    atoms = []
    aromatic_bonds = []
    bonds = []
    c=Counter()
    mol = Chem.MolFromSmiles(smi, sanitize = False)
    Chem.SanitizeMol(mol,sanitizeOps=Chem.SanitizeFlags.SANITIZE_ALL^Chem.SanitizeFlags.SANITIZE_KEKULIZE)#, catchErrors=False)

    atomNumbers = mol.GetNumAtoms()
    
    #for num, atom in enumerate(mol.GetAtoms()):
    #    atoms.append("v %s %s" %(num,atom.GetAtomicNum()))
    
    numbonds = len(mol.GetBonds())
    for i in range(numbonds):
        #print a.GetAtomWithIdx(1).GetAtomicNum()
        start = mol.GetBondWithIdx(i).GetBeginAtomIdx()
        end = mol.GetBondWithIdx(i).GetEndAtomIdx()
        bond = mol.GetBondBetweenAtoms(start,end).GetBondTypeAsDouble()
        if bond == 1.5:
            #bond = 5 
	    aromatic_bonds.append(start)
	    aromatic_bonds.append(end)
    c=Counter(aromatic_bonds)
    if c.most_common()[-1][1] > 1 :
	#mol = Chem.AddHs(mol)
	mol1 = Chem.MolToSmiles(mol)
	print "%s\t%s" %(mol1, name)
    
        #bonds.append ("e %s %s %s" %(start, end, str(int(bond))))
    
    #return atoms,bonds

def main ():
    flag = 0
    for line in sys.stdin:
	#print line
	if line.count("[") > 0 :
	    flag = 0
	elif line[0] == "\n":
	    flag = 1
	if flag ==1 and line[0] != "\n":
	    smi, name = line.rstrip().split(" ", 1)
	    open_aromatic_ring_test(smi,name)
	    
	    
if __name__ == "__main__" :
    main()



#import subprocess
#
#p = subprocess.Popen(["/media/tsonko/DATA/tt/24k/scripts/fffff.py"], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
#out1 = p.stdout.read()
#out = p.stderr.read()
#print out
#print out1

