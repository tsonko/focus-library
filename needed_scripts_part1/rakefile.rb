################################################################################
# Globals
################################################################################
#MAIN_FOLDER = '/media/tsonko/extra500g/tt/22_rake'
SCRIPT_FOLDER = "/media/tsonko/DATA/tt/24k/scripts/git_ready"
NEEDED_DATA =  "/media/tsonko/DATA/tt/needed_data"
GASTON = "/media/tsonko/DATA/tt/24k/scripts/gaston-1.2-re"
FILE_FORMAT = "can"

#gaston arguments explantaion:
#max number of atoms 21 
#substructures are reported with minumum frequency of 50
GASTON_ARGUMENTS= "-m 21 50" 

#g2m
MIN_NUM_ATOMS = "10"
CPUS = "14"
UNKNOWN = "DB"


###############################################################################
#####FIRST PART EXTRACT FORM CHEMBL, GASTON, AND DICTIONARY GENERATION#########
############################################################################### 


#extracting smilis/can from local version of Chemble.
#Chemble nees to be prepared as it described in
#/home/tsonko/projects/deorphan/README.txt
rule ".can" do |t|
    #sh "#{SCRIPT_FOLDER}/n_parallelize/extractSmiles.py > #{t.name}"
    sh "#{SCRIPT_FOLDER}/extractSmiles.py > #{t.name}"
end




##make cans unique before thepipeline
rule ".ucan" => ".can" do |t|
    sh "sort -u #{t.source} > #{t.name}"
end

############################################################
######################## GASTON START ######################
################## Require installed RDKIT #################
############################################################


#Mol2graph
#it also generates ids file for later use.
#it requires rdkit instalation (python API)
rule ".m2g" => ".ucan" do |t|
   sh "#{SCRIPT_FOLDER}/molGraph/m2g_rdkit-h.py -i #{t.source} > #{t.name}"
end

#Gaston
rule ".gaston" => ".m2g" do |t|
  sh "#{GASTON}/gaston #{GASTON_ARGUMENTS} #{t.source} #{t.name}" # gaston arguments are in the Global variables Section
#  sh "time #{GASTON}/gaston #{GASTON_ARGUMENTS} #{t.source} #{t.name}" #provides the running time in STOUT
end

#Graph2Mol
rule ".g2m_speedUp2" => ".gaston" do |t|
  #sh "#{SCRIPT_FOLDER}/molGraph/g2m_speedUp2.py -i #{t.source} -f #{FILE_FORMAT} -n #{MIN_NUM_ATOMS} -d ids -c #{CPUS} > #{t.name}"
  sh "#{SCRIPT_FOLDER}/molGraph/g2m_speedUp2.py -i #{t.source}  -n #{MIN_NUM_ATOMS} -c #{CPUS} > #{t.name}"
#  puts "#{SCRIPT_FOLDER}/molGraph/g2m.py -i #{t.source} -f #{FILE_FORMAT} -n #{MIN_NUM_ATOMS} -d ids -s #{CPUS} > #{t.name}"
end


#filter fragments for correct substructures
rule ".filtered_subs1" => ".g2m_speedUp2" do |t|
   sh "#{SCRIPT_FOLDER}/filter_corect_smiles-part1.py -i #{t.source} -o #{t.name}" 
end


#filter fragments for correct substructures
rule ".filtered_subs2" => ".g2m_speedUp2" do |t|

   sh "#{SCRIPT_FOLDER}/filter_corect_smiles-part1.py -i #{t.source} -o filter_fragment1 2>&1 | #{SCRIPT_FOLDER}/filter_corect_smiles-part2.py > #{t.name}"

end

#task :default do |t,arg|
#  sh "rake 
#end



################################################
############ dictionary creation ###############
################################################


#CREATE PROTEIN CLASSIFICATION DICTIONARY 
#clasify all of the parents Peptide binding vs non peptide binding

#find all of the parents uniprot accession numbers form the chemble table which is created prior this moment.
rule ".dbSearch_combined" => ".ucan" do |t|
    sh "#{SCRIPT_FOLDER}/dbSearch_combine.py -i #{t.source} -o #{t.name} -c #{CPUS}"
end


#extract the unique accession numbers for compounds parent targets
rule ".acc" =>  ".dbSearch_combined" do |t|
   sh "cut -f5 #{t.source} | tr ',' '\n' | sort -u > #{t.name}"
end


#due to a problem in GPCRdb one cannot search in there by uniprot accession number but only by lower case uniprot entry_name.
#hence in the next step we fish the entry_name ids from uniprot trough uniprot API.
#one can also do that using the web-gui interface which is described in the raadme doc file. Which is a better way for long list
#reasons. 

#search in uniprot for the accession numbers
rule ".uniprot" => ".acc" do |t|
    sh "perl #{SCRIPT_FOLDER}/perl.pl #{t.source} > #{t.name}"
end


def entry_name_dependency(input_file_name)
   file_name = input_file_name.chomp('.entry_names')
   return ["#{file_name}.acc", "#{file_name}.uniprot"]
end


rule ".entry_names"  => lambda {|i| entry_name_dependency(i)} do |t|
   acc_file, uniprot_file =entry_name_dependency(t.name)
   sh "paste #{acc_file} #{acc_file} > tempfile" 
   #sh "grep '^AC' #{uniprot_file} | sed 's/ \\+/\t/g' | cut -f2 | sed 's/;//g' | paste tempfile - > #{t.name}"
   sh "grep '^ID' #{uniprot_file} | sed 's/ \\+/\t/g' | cut -f2,3 | sed 's/;//g' | paste tempfile  - > #{t.name}"
end

rule ".dict_txt" => ".entry_names" do |t| 
     sh "#{SCRIPT_FOLDER}/accession2dict.py #{t.source} | sed 's/<sub>//g' | sed 's/<\\/sub>//g' | sed 's/<i>//g' | sed 's/<\\/i>//g'| sed 's/;/ /g' | sed 's/&//g' > #{t.name}"
     sh " cp #{t.name} #{NEEDED_DATA}/#{t.name}"
end
######################################################
############ END DICTIONARY CREATION #################
######################################################

#translate and clasify the parrent compounds targets protein and protein family names aswell as peptide and nonpeptide binding. 
def translated_dependency(input_file_name)
   file_name = input_file_name.chomp('.translated')
   return ["#{file_name}.dbSearch_combined", "#{file_name}.dict_txt"]
end


#translated
#translate and clasify the accession numbers to protein and protein family names aswell as peptide and nonpeptide binding. 

rule ".translated" => lambda {|i| translated_dependency(i)} do |t|
   dbsearch, dict = translated_dependency(t.name)
    sh "#{SCRIPT_FOLDER}/translate_combined_unique.py -i #{dbsearch} -d #{dict} > #{t.name}"
end


################################################################################
############################### END OF PART ONE ################################
################################################################################


#NOTE README
#To continue further in the project an essential GUI step should take plase here.
#Rename the .dbSearch_combined to _dbSearched_combined.csv and .filtered to _filtered.csv files 
#Load the renamed files in InstantJChem from ChemAxon and preform substructure search 
#using the _dbSearched_combined.csv for target and _filtered.csv for query. 
#With this step we found from which parent compounds we have fragments in the list survived 
#from Gaston. This knowledge will allow us to calculate selectivity ration later in the process. 

#NOTE NOTE NOTE NOTE NOTE NOTE
#the substructure search in instantJChem(IJC) as well as the information extracted from IJC is descrobed 
#in the screen shots in the IJC-readme.doc
#Alternatively and to a great cost in time, one can have a look on the next Function and rule.  

##Jchem searcj 

#def marvin_and_db_dependency(input_file_name)
#   marvin_file, db_file = input_file_name.chomp('.sdf_temp').split('_vs_')
#   return ["#{marvin_file}.mrv", "#{db_file}.indexed"]
#end

#rule ".sdf_temp"  => lambda {|i| marvin_and_db_dependency(i)} do |t|
#   query_file, db_name = marvin_and_db_dependency(t.name)
#   sh "/home/tsonko/ChemAxon/JChem/bin/jcsearch -q #{query_file} -f sdf -o #{t.name} #{db_name}"   
#   #sh "/home/tsonko/ChemAxon/JChem/bin/jcsearch -q #{query_file} -f :M sdf -o #{t.name} #{db_name}"   
#end

################################################################################
############################ START OF PART 2 ###################################
################################################################################

#To continue one should have a file that look as the template in the next line
#SMILES\tID\tnumber_overlaping_targets\ttarget_ccession_number
#The fomrat described above is the format in .dbSearch_combined file, hence the filename should be 
#          .subs_dbSearch_combined




#redundancy reduction step1
#rule ".nc1" => ".g2m_speedUp2" do |t|
#  sh "#{SCRIPT_FOLDER}/reduce_redundancy_before_analyze2.py -i #{t.source} > #{t.name}"
#end 
#
#
##redundancy reduction step2
#rule ".nc2" => ".nc1" do |t|
#  sh "#{SCRIPT_FOLDER}/reduce_redundancy_before_analyze2.py -i #{t.source} > #{t.name}"
#end
#
##redundancy reduction step3
#rule ".nc3" => ".nc2" do |t|
#  sh "#{SCRIPT_FOLDER}/reduce_redundancy_before_analyze2.py -i #{t.source} > #{t.name}"
#end

##create index/ID and tab separated file
#rule ".indexed" => "g2m_speedUp2" do |t|
#   sh "#{SCRIPT_FOLDER}/indexing.py -i #{t.source} > #{t.name}"
#end
#
##create index/ID and tab separated file
#rule ".indexed" => ".nc3" do |t|
#   sh "#{SCRIPT_FOLDER}/indexing.py -i #{t.source} > #{t.name}"
#end
#
##marvin1_vs_list.sdf
#def marvin_and_db_dependency(input_file_name)
#   marvin_file, db_file = input_file_name.chomp('.sdf_temp').split('_vs_')
#   return ["#{marvin_file}.mrv", "#{db_file}.indexed"]
#end
#
##Jchem searcj 
#rule ".sdf_temp"  => lambda {|i| marvin_and_db_dependency(i)} do |t|
#   query_file, db_name = marvin_and_db_dependency(t.name)
#   sh "/home/tsonko/ChemAxon/JChem/bin/jcsearch -q #{query_file} -f sdf -o #{t.name} #{db_name}"   
#   #sh "/home/tsonko/ChemAxon/JChem/bin/jcsearch -q #{query_file} -f :M sdf -o #{t.name} #{db_name}"   
#end
#
#def concatinated_sdf_dependency(input_file_name)
#   my_re = /(.+?)(\d+)-(\d+)_vs_(.+?)\.sdf$/
#   query_name, _from, _to, db_name = my_re.match(input_file_name)[1..-1]
#   _ret = []
#   
#   Integer(_from).upto(Integer(_to)) do |d|
#      _ret << "#{query_name}#{d}_vs_#{db_name}.sdf_temp"
#   end
#   return _ret
#end
#
##marvin1-4_vs_list.sdf
#rule ".sdf" => lambda {|i| concatinated_sdf_dependency(i)} do |t|
#   files = concatinated_sdf_dependency(t.name)
#   sh "cat #{files.join(' ')} > #{t.name}"
#end
#
##obabel sdf to smile get the id list
#rule ".ids" => ".sdf" do |t|
#   sh "obabel -i sdf #{t.source} -o smi | cut -f2 | sort -un > #{t.name}"
#end
#
##extract ids from indexed
##marvin1-4_vs_list.filtered 
#def id_extraction_dependency(input_filename)
#   db_name = input_filename.chomp('.filtered').split('_vs_')[-1]
#   db_name = "#{db_name}.indexed"
#   idFile = input_filename.chomp('.filtered')
#   return ["#{idFile}.ids", db_name]
#end   
#
#rule ".filtered" => lambda{|i| id_extraction_dependency(i)} do |t|
#   idFile, db_name = id_extraction_dependency(t.name)
#   sh "#{SCRIPT_FOLDER}/extract_id_from_smiles.py -i #{db_name} -d #{idFile} > #{t.name}"
#   #puts "#{SCRIPT_FOLDER}/extract_id_from_smiles -i #{db_name} -d #{idFile} > #{t.name}"
#end
#

#postfiltering
#rule ".pf" => ".g2m" do |t|
#  sh "#{SCRIPT_FOLDER}/parallel/postFiltering.py -i #{t.source} -o #{t.name} -c #{CPUS}"
#  puts "#{SCRIPT_FOLDER}/parallel/postFiltering.py -i #{t.source} -o #{t.name} -c #{CPUS}"
#end

#database search
#rule ".dbSearch" => ".pf" do |t|
#rule ".dbSearch" => ".g2m" do |t

#expected: marvin1-4_vs_list.dbSearched
rule ".dbSearched" => ".filtered" do |t|
   sh "#{SCRIPT_FOLDER}/n_parallelize/dbSearch.py -i #{t.source} -o #{t.name} -c #{CPUS}"
end


#Search in the CHEMBL for the targets which these compounds bind to
rule ".dbsearchedNonPep" => ".filtered" do |t|
   sh "#{SCRIPT_FOLDER}/n_parallelize/dbsearchNonPeptide.py -i #{t.source} -o #{t.name} -c #{CPUS}"
end

##analyze
#rule ".analyzed" => ".dbSearch" do |t|
##  sh "#{SCRIPT_FOLDER}/parallel/analyze.py -i #{t.soruce} -o #{t.name} -c #{CPUS}"
#  sh "#{SCRIPT_FOLDER}/parallel/analyze.py -i #{t.source} -o #{t.name} -c #{CPUS}"
#end

#translated
rule ".translatedPep" => ".dbSearched" do |t|
   sh "#{SCRIPT_FOLDER}/translate_1.py -i #{t.source} -d #{NEEDED_DATA}/dictPeptide.csv > #{t.name}"
end

rule ".translatedNonPep" => ".dbsearchedNonPep" do |t|
   sh "#{SCRIPT_FOLDER}/translate_1.py -i #{t.source} -d #{NEEDED_DATA}/dictNonPeptide.csv > #{t.name}"
end

#rule ".sdf" => ".translatedPep" do |t|
#   sh "cut -f1,2 #{t.source} | obabel -ismi - -osdf -O #{t.name} -h -r -e -p 7.4 --gen2D"
#end
#
rule ".sdf" => ".translatedNonPep" do |t|
   sh "cut -f1,2 #{t.source} | obabel -ismi - -osdf -O #{t.name} -h -r -e -p 7.4 --gen2D"
end

#perhaps a list of FPs should be placed or a funktion. also a proper pat in the settings line
rule ".jcm" => ".sdf" do |t|
   #sh "java -jar ~/projects/vs/scripts_tobias/jCMapperCLI.jar -f #{t.source} -c ECFP -d 4 -ff FULL_CSV -o #{t.name}"
   sh "java -jar ~/projects/vs/scripts_tobias/jCMapperCLI.jar -f #{t.source} -c AT2D -d 28 -ff FULL_CSV -o #{t.name}"
end



#task :html => %W[ch1.hit ch2.hit ch3.hit]


#%W[marvin1.1.mrv  marvin1.2.mrv  marvin2.1.mrv  marvin2.2.mrv].each do |f|
#  puts f
#end
#task :name, [:first_name, :last_name] do |t, args|
#  args.with_defaults(:first_name => "John", :last_name => "Dough")
#  puts "First name is #{args.first_name}"
#  puts "Last  name is #{args.last_name}"
#end



