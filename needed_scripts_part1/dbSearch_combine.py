#!/usr/bin/python
import getopt
import sys
import os, errno
import parallelize
from sets import Set

def processData(line, con):
    """
    Searches the ChEMBL database for activities with the supplied molregnos
    """
    # target and hit counts
    targets = Set()
    accessions = Set()
    chembl_id = Set()

    # parse input string, "SMILES hit1,hit2,..,hitN"
    #splitInput = line.rstrip('\n').split('\t')
    splitInput = line.rstrip('\n').split()
    smiles = splitInput[0]
    title = splitInput[1]
    hits = splitInput[-1].split(',')

    # read the output file
    output = list()

    # loop through the hits and add them to the output
    for hitId in hits:
        # open a cursor
        cur = con.cursor()
        
        #fetch all targets that this substructure hits
        #query = "SELECT target_id FROM filterKinaseHuman10m WHERE molregno="+hitId
        #query = "SELECT target_id, accession FROM filterGpcrHumanPeptide1m WHERE molregno="+hitId
        query = "SELECT target_id, accession FROM filterGpcrMammalia1m WHERE molregno="+hitId
        #query = "SELECT target_id, accession FROM filterGpcrHuman1m WHERE molregno="+hitId
        #query = "SELECT target_id, accession, molecule_chembl_id FROM filterGpcrHuman1m WHERE molregno="+hitId
#        query = "SELECT target_id, smiles, molecule_chembl_id FROM filterGpcrHuman1m WHERE molregno="+hitId
        #query = "SELECT target_id FROM filterGpcrHuman1m WHERE molregno="+hitId
        cur.execute(query)
        numrows = int(cur.rowcount)

        # go through rows
        for i in range(numrows):
            row = cur.fetchone()
            targets.add(str(row[0]))
            accessions.add(str(row[1]))
            #chembl_id.add(str(row[2]))
            #print "%s\t%s\t%s" %(str(row[1]),str(row[2]),hitId)
            #targetId = str(row[0])
            # update target counter
            #if targetId not in targets:
            #    targets.append(str(targetId))

        # close cursor
        cur.close()

    # print output, "numberOfTargets;numberOfHits;smiles;hit1,hit2,..,hitN;target1,target2,..,targetN"
    #hitString = ','.join(hits)
    targetString = ','.join(targets)
    accessionsString = ','.join(accessions)
    chembl_idString = ','.join(chembl_id)
    outputString = smiles+'\t'+title+'\t'+str(len(targets))+'\t'+targetString+'\t'+accessionsString
#    outputString = smiles+'\t'+title+'\t'+chembl_idString+'\t'+str(len(targets))+'\t'+targetString+'\t'+accessionsString
    output.append(outputString)

    # return the hits
    return output

#remove output filemane if exists
def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e:
        if e.errno!=errno.ENOENT:
            raise
    
# help message
def usage():
    print "-h\t-help\t\tPrint this message"
    print "-i\t-input\t\tPath to input file"
    print "-o\t-output\t\tPath to output file"
    print "-c\t-cpu\tNumber of CPUs to use"

def main (argv):
    try:
        opts, args = getopt.getopt(argv, "i:o:c:h", ["input", "output", "cpu", "help"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    # default values
    inputFileName = False
    outputFileName = False
    #dbTable="pep"
    cpus = 1
    
    # arguments
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-i", "--input"):
            inputFileName = arg
        #elif opt in ("-d", "--dbTable"):
        #    dbTable = arg
        elif opt in ("-o", "--output"):
            outputFileName = arg
        elif opt in ("-c", "--cpu"):
            cpus = arg
    
    
    
    # run
    silentremove(outputFileName)
    functionName = os.path.basename(__file__).split('.')[0]
    parallelize.prepareInput(functionName, inputFileName, outputFileName, cpus)
    
if __name__ == "__main__":
    main(sys.argv[1:])
