#!/usr/bin/python
import getopt
import sys
import MySQLdb as mdb

def extractSmiles():
    """
    Fetches SMILES string from a custom table generated from the ChEMBL
    database and writes them out to text file with their molregno (ids).
    As it is works with CHEMBL20 with and an extra created table. 
    See MySQL script in the same folder for creating such table.
    """

    # search for the ligand in chembl
    con = mdb.connect('localhost', 'chembl_22', 'chembl_22', 'chembl_22')
    with con:
        cur = con.cursor()
        # find the interal id of the comoound (not the same as the chembl id for some reason)
        #query = ("SELECT smiles, molregno FROM filterGpcrHumanPeptide1m;") 
        #query = ("SELECT smiles, molregno FROM filterGpcrHumanNONPeptide1m;") 
        query = ("SELECT smiles, molregno FROM filterGpcrMammalia1m;") 
        cur.execute(query)
        numrows = int(cur.rowcount)
        for i in range(numrows):
            row = cur.fetchone()
            print str(row[0])+" "+str(row[1])
    
# help message
def usage():
    print "-h\t-help\t\tPrint this message"
def main (argv):
    try:
        opts, args = getopt.getopt(argv, "h", ["help"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    # arguments
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
    
    # run
    extractSmiles()
    
if __name__ == "__main__":
    main(sys.argv[1:])
