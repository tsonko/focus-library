#!/usr/bin/python
import sys
import csv
from sets import Set
import requests
import urllib

'''
The scritp clasify the compound targets using the information from GPCRdb and the entry names from uniprot. 
The input for this script is a tab separated .csv file that is output from dbSearch_combined.py script where the
last colum contain one or many uniprot accession numbers.
The script also works with a file that contains the uniprot accession numbers in one column.

'''
def get_uniprot_entry_name (up_id):
    #    print (up_id)

    #get the 'entry name' field for given uniprot id
    #'entry name' is a protein id in gpcrdb
    url = "http://www.uniprot.org/uniprot/?query=accession:%s&columns=entry name&format=tab" %up_id

    #used urllib, urllib2 throws an error here for some reason
    response = urllib.urlopen(url)
    page = response.readlines()
    return page[1].strip().lower()


def read_input_file(fname):
    csvfile = open(fname)
    try:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)
        reader = csv.reader(csvfile, dialect)
        a = Set()
        for row in reader:
            temp = str(row[-1]).split(',')
            for i in temp:
                a.add(i)
    #    print a
    except csv.Error:
        f = open(fname)
        a = set(f.read().split('\n')[:-1])

    csvfile.close()
 #   print a
    return a
    

def main (argv):
    outputFile = argv[1]
    inputFile= argv[0]
    foo = open(outputFile, 'w')
    accs = read_input_file(inputFile)
    for acc in accs: 
        entry_name = get_uniprot_entry_name(acc)
        
        url = "http://gpcrdb.org/services/protein/"+entry_name
#            #url = "http://test.gpcrdb.org:80/services/protein/accession/" + entry_name
        slug_data = requests.get(url).json()
        try :
             slug = slug_data['family']
             url2 = "http://test.gpcrdb.org/services/proteinfamily/"+slug
             family_data=requests.get(url2).json()
             test = slug.split('_')[1]
             if test == '002':
                 print '%s\t%s\t%s\t%s' %(acc.upper(), family_data['parent']['name'],family_data['name'],'P')
             else:
                 print '%s\t%s\t%s\t%s' %(acc.upper(), family_data['parent']['name'],family_data['name'],'N')

        except KeyError:
              foo.write('\"%s\"\t ' %(acc.upper())) 


if __name__ == "__main__":
    main(sys.argv[1:])
