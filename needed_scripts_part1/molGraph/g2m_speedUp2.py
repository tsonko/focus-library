#!/usr/bin/python
import getopt
import gc
import sys
import molgraph2
import math
from multiprocessing import Process, Queue


'''
Read output from Gaston(graphs) and convert them to SMILES. 
Filter them based on minimum number of atoms "-n" flag
Filter them based on minimum number of rings line 70
'''

def splitInChunks(l, n):
    """
    Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def submitWorker(queue, graphs, fileFormat, minAtomNum):
    results = list()
    for graph in graphs:
        molecule = molgraph2.graphToMolecule(graph[0], graph[1], graph[2], graph[3], fileFormat, minAtomNum)
        if type(molecule) is list:
            results = results + molecule
    queue.put(results)

def convertStructures(inputFileName, fileFormat, minAtomNum, idFileName, cpus):

    # init variables
    frequency = 0
    graphs = list()
    nodes = list()
    edges = list()
    ids = list()

    # read id file
    if idFileName:
        with open(idFileName) as idFile:
            # read the first line of the file
            idLine = idFile.readlines()[0].strip('\n')

            # read molregnos into array
            molregnos = idLine.split(' ')
    
    # read graph file
    with open(inputFileName) as inputFile:
        for i,line in enumerate(inputFile):
            #control the memory usage 
            if len(graphs)<int(cpus)*10000:
               
                # if a start of a new graph is found, init the nodes, edges, and tids
                if line[0] == 't':
                    nodes = list()
                    edges = list()
                    ids = list()
                    t, tnum = line.rstrip('\n').split(' ')
                else:
                    # nodes
                    if line[0] == 'v':
                        nodeValues = line[:-1].split(' ')
                        nodes.append(nodeValues[1:]) 
                    # edges
                    elif line[0] == 'e':
                        edgeValues = line[:-1].split(' ')
                        edges.append(edgeValues[1:])

                    # value -1 this coresponds to 2 or more rings. Change value to 0 to return 1 or more rings
                    if int(len(nodes)) > int(minAtomNum) and int(len(nodes)) - int(len(edges)) <=-1 :
                        # tids
                        if line[0] == ':':
                            #tids = line[:-1].split(' ')[1:]
                            tids = line.rstrip('\n').split(' ')[1:]
        
                            # read id file
                            if idFileName:
                                # lookup the molregnos
                                for tid in tids:
                                    ids.append(molregnos[int(tid)])
                            # add graph to array
                            graph = [nodes, edges, ids, tnum]
                            graphs.append(graph)
                        
                        
                        # convert graph to molecule
                        #molgraph.graphToMolecule(nodes, edges, ids, fileFormat, minAtomNum)
            elif len(graphs) == int(cpus)*10000:
                # split the dataset into equal pieces
                
                chunks = list(splitInChunks(graphs, int(math.ceil(len(graphs)/float(cpus)))))
            
                # run in parallel
                results = list()
                q = Queue()
                procs = list()
                for chunk in chunks:
                    p = Process(target=submitWorker, args=(q, chunk, fileFormat, minAtomNum))
                    procs.append(p)
                    p.start()
            
                for p in procs:
                    results = results + q.get()
            
                for p in procs:
                    p.join()
            
                print "\n".join(results)
                graphs = []
                #gc.collect()
            # split the dataset into equal pieces
        if len(graphs)!=0:
        # getting smaller than 1393188 (line 44 )cunks if any.
            chunks = list(splitInChunks(graphs, int(math.ceil(len(graphs)/float(cpus)))))
            
            # run in parallel
            results = list()
            q = Queue()
            procs = list()
            for chunk in chunks:
                p = Process(target=submitWorker, args=(q, chunk, fileFormat, minAtomNum))
                procs.append(p)
                p.start()
            
            for p in procs:
                results = results + q.get()
            
            for p in procs:
                p.join()
            
            print "\n".join(results)
            graphs = []
# help message
def usage():
    print "Usage: ./g2m.py -i inputFileName -f fileFormat [-n]"
    print "-n\tMinimum number of atoms in output structures"
    print "-d\tPath to id file" #not needed for now is used substructure search for finding parents of the substructures 
    print "Format may be anything supported by OpenBabel"

def main (argv):
    try:
        opts, args = getopt.getopt(argv, "i:f:n:d:c:h", ["input", "format", "minatoms", "idfile", "cpus", "help"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    # default values
    inputFileName = False
    fileFormat = "smi"
    minAtomNum = 0
    idFileName = False
    cpus = 1
    
    # arguments
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-i", "--input"):
            inputFileName = arg
        elif opt in ("-f", "--format"):
            fileFormat = arg
        elif opt in ("-n", "--minatoms"):
            minAtomNum = arg
        elif opt in ("-d", "--idfile"):
            idFileName = arg
        elif opt in ("-c", "--cpus"):
            cpus = arg
    # run
    convertStructures(inputFileName, fileFormat, minAtomNum, idFileName, cpus)
    
if __name__ == "__main__":
    main(sys.argv[1:])
