#!/usr/bin/python
import pybel
import openbabel
import getopt
import sys
import inspect
from rdkit import Chem


def graphToMolecule(nodes, edges, ids, tnum, fileFormat, minAtomNum):

    # init new molecule
    mol = Chem.Mol()
    rw = Chem.rdchem.EditableMol(mol)

    # loop through and add the nodes to the molecule
    for node in nodes:
        
        # get the atomic number of the node (atom)
        #atomicNum = int(node[1])
        atom=Chem.Atom(int(node[1]))
        rw.AddAtom(atom)

    # loop through and add the edges to the molecule
    for edge in edges:
        if int(edge[2]) == 5:
            rw.AddBond(int(edge[0]), int(edge[1]), Chem.rdchem.BondType.values[12])
        else:
            rw.AddBond(int(edge[0]),int(edge[1]), Chem.rdchem.BondType.values[int(edge[2])])        

    # convert to a editable molecule to molecule object (for writing out the molecule)
    mol = Chem.rdchem.EditableMol.GetMol(rw)

    ## check if the molecule exceeds the minimum number of atoms
    
    #atomNum = Chem.Mol.GetNumAtoms(mol)
    #if atomNum  > int(minAtomNum):
        # write out the molecule
        #print mol.write(fileFormat)[:-2]+" "+",".join(ids) # serial
        #mol.draw(False,"img/"+str(len(nodes))+".png")
        
        # using for testing purposes. It records the substructure number after the smile and separated with " "
        
    #print "\n".join([Chem.MolToSmiles(mol).strip()+" "+tnum+" "+",".join(ids)])
        #print Chem.MolToSmiles(mol)[0]
    return list([Chem.MolToSmiles(mol).strip()+" "+tnum+" "+",".join(ids)]) 
    #return list([mol.write(fileFormat).strip()+" "+",".join(ids)]) # parallel
