#!/usr/bin/env python
import sys
import getopt
from rdkit import Chem

'''
Require instalation for RDKIT
The script takes as an input any SMILES format file and produces a graph 
file used for input in Gaston. All of the Hydorgen atoms are removed 
from the graphs and the aromatic bonds are recordet as they are.
The output is in STDOUT
Format:
SMILES ID
'''

def m2g(smi):
    atoms = []
    bonds = []
    mol = Chem.MolFromSmiles(smi, sanitize = False)
    atomNumbers = mol.GetNumAtoms()
    
    for num, atom in enumerate(mol.GetAtoms()):
        atoms.append("v %s %s" %(num,atom.GetAtomicNum()))
    numbonds = len(mol.GetBonds())
    for i in range(numbonds):
        start = mol.GetBondWithIdx(i).GetBeginAtomIdx()
        end = mol.GetBondWithIdx(i).GetEndAtomIdx()
        bond = mol.GetBondBetweenAtoms(start,end).GetBondTypeAsDouble()
        if bond == 1.5:
            bond = 5 
    
        bonds.append ("e %s %s %s" %(start, end, str(int(bond))))
    
    return atoms,bonds 

#read the input filename and call def m2g()
def readfile(filename):
    
    with open(filename, "r") as inFile:
        molecules = inFile.readlines()
    
    ids = list()

    #loop through molecules and print graphs

    for i,molecule in enumerate (molecules):
        splitMolecule = molecule.rstrip().split()
        molstr = splitMolecule[0]
        molregno = splitMolecule[1]

        #pritn tid line 
        print "t # " + str(i)
        
        #call def m2g()
        #translate molecule to graph + remove Hs
        a = []
        b = []
        a, b = m2g(molstr)
        for i in a :
            print i
        for i in b :
            print i 

        #save id
        ids.append(molregno)
        

    #it is not clear if we should keep the ids
    #while we do not use them for anything.
    with open("ids", "w")as idFile:
        idFile.write(' '.join(ids))

def usage():
    print "usage: $PAtH/m2g.py -i input.file(can/smi)"
    print "-i input file"

def main (argv):

    try:
        opts,args = getopt.getopt(argv,"i:f", ["input", "format"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    #default values 
    infile = False
    
    #arguments
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-i", "--input"):
            infile = arg


    #run
    readfile (infile)

if __name__ == "__main__":
    main(sys.argv[1:])

