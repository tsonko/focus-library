#!/usr/bin/env python
import os
import sys
import getopt
from rdkit import Chem
from collections import Counter

# "Usage: $PATH/filter_corect_smiles-part1.py -i inputFileName -o outputFileNmae 2>&1 | $PATH/filter_corect_smiles-part2.py > second outputilename"
# The first output contains all of the fragments that are chemically correct. However, many of the rejected ones are tested if they contain open rings.
#if not they are reported in the secound output file.
#2>&1

def test_smi(ar, outputFilename):
    with open (outputFilename, "w") as foo:
    
	for smi in ar:
	    mol1 = Chem.MolFromSmiles(smi[0], sanitize = False)
	    mol2 = Chem.MolFromSmiles(smi[0])

            if mol2!= None:
		a = Chem.MolToSmiles(mol1)
		b = Chem.MolToSmiles(mol2)
		if a==b:
		    #mol_list.append(mol2)
		    foo.write("\t".join([b,smi[1]])+"\n")
	    else:
		mol2 = Chem.MolFromSmiles(smi[0], sanitize = False)
		b = Chem.MolToSmiles(mol2)
		print " ".join([b,smi[1]])

    #print smiles_list[:10]
    #print index
    #Draw.MolsToGridImage([x for x in mol_list], molsPerRow=6).show()
    
##read smiles file and return AoA [smi,name]
def read_smi_file(inputFileName):
    ar 	= []
    with open(inputFileName, 'r') as fii:
        for line in fii:
            temp = line.rstrip().split(' ',1)
            ar.append(temp)
    return ar

# help message
def usage():
    print "Usage: $PATH/ffffff.py -i inputFileName -o outputFileNmae 2>&1 | $PATH/filter_correct_part2.py > second outputilename"
    print "-i\tPath and name of the input file (xxx.g2m_speedp2)"
    print "-o\tPath and name for the output file"

def main (argv):
    try:
        opts, args = getopt.getopt(argv, "i:o:h", ["input", "output", "help"]) 
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    # default values
    inputFileName = False
    outputFileName = False
    #minAtomNum = 0
    #idFileName = False
    #cpus = 1
    
    # arguments
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-i", "--input"):
            inputFileName = arg
        elif opt in ("-o", "--output"):
            outputFileName = arg

    ##run    
    #ar = read_smi_file("/media/tsonko/DATA/tt/combined_class_A/combined_gaston/pipeline_small_scale_test/1")
    #ar = read_smi_file("/media/tsonko/DATA/tt/combined_class_A/combined_gaston/pipeline_small_scale_test/results-hit.smi")
    ar = read_smi_file(inputFileName)
    test_smi(ar, outputFileName)
    
    
if __name__ == "__main__":
    main(sys.argv[1:])    
