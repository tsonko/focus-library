# __Focus Libraries__ #
------------------------

In this wiki I will have some notes on how the compound libraries were selected and the workflow used in the process.

## Stages ##
 
There are several stages in the workflow and I will try to document each one of them as accurately as possible. To begin with main stages are:

1. Creating a local ChEMBL data base with the needed structure
2. Gaston (Molecular fragmentation and initial filtering)
3. Filtering Gaston output
4. Create classification dictionary (peptide/nonpeptide/exceptions)
5. Further filtering and classification with the fragments from Gaston and preference ration calculation (based on the parental targets)
6. Fragment characterization 
7. Enamine download and loaded in InstantJchem (IJC) extract smi. (or use maestro to convert enamine.sdf to enamine.smi)
8. Filtering Enamine data set (MW = 300-350; 300-450; 300-500) for focus and general purpose libraries
9. Substructure search - fragments as query, Enamine subset as target
10. Similarity search - fragments as query, Enamine subset as target
11. Library selection 

## Creating a local ChEMBL database and custom table used later in the process (stage 1) ##

The selection and the code in the script examples is generated for the version **ChEMBL_22 database**. Before before continue and use the provided code below find the schematic .png file reference-style link for the newest version [here][chembl] and compare with the [chembl_22.png][22] to check if the needed tables(from the mySQL query below) are in the correct places. If not the query should be changed accordingly. 

__NOTE__: The target classification tables are not be visible on the chemble_22.png but they are there. It is written that this type of classification is discontinued but it is still there in version 22. One can see where they are located in version 19 before. 

### Creating a local copy for ChEMBL ###

1. Locate and download the newest chemblXX_mysql.tar.gz in the bottom folder [here][chembl]. 
2. Decompress the downloaded file (notice the location).
3. Create a new database in the local mySQL.
    - Open a terminal and input the next code 
    
            mysql -u root -p
            <root_pass>

            SHOW DATABASES;
            CREATE DATABASE chembl_XX;
            GRANT ALL PRIVILEGES ON chembl_XX.* TO chembl_XX@localhost IDENTIFIED BY “Password”

    __NOTE:__ Take note of the password and the name in this case - chembl_22 and Password.

4. Load the mySQL dump file dowloaded from ChEMBL.
    - In the terminal navigate to the location of the downloaded mysql file and input the following code (note that it will take several hours):    
            
            mysql -u username -p database_name < file.sql

5. Create custom table with the following script/query:

```
#!mysql
CREATE TABLE filterGpcrMammalia1m 
SELECT molecule_dictionary.chembl_id AS molecule_chembl_id, 
    molecule_dictionary.molregno AS molregno, 
    compound_structures.canonical_smiles AS smiles, 
    molecule_dictionary.molecule_type AS molecule_type, 
    assays.assay_id AS assay_id, 
    assays.description AS assay_description, 
    assays.assay_type AS assay_type, 
    assays.confidence_score AS assay_confidence_score, 
    activities.standard_type AS activity_std_type, 
    activities.standard_value AS activity_std_value, 
    activities.standard_units AS activity_std_units,
    activities.published_type AS activity_pub_type, 
    activities.published_value AS activity_pub_value, 
    activities.published_units AS activity_pub_units, 
    target_dictionary.tid AS target_id, 
    target_dictionary.chembl_id AS target_chembl_id, 
    component_sequences.accession AS accession, 
    component_sequences.description AS target_description, 
    target_dictionary.organism AS organism, 
    protein_family_classification.l1 AS target_class_l1, 
    protein_family_classification.l2 AS target_class_l2, 
    protein_family_classification.l3 AS target_class_l3, 
    protein_family_classification.l4 AS target_class_l4,
    protein_family_classification.l5 AS target_class_l5,
    protein_family_classification.l6 AS target_class_l6,
    protein_family_classification.l7 AS target_class_l7,
    protein_family_classification.l8 AS target_class_l8, 
    organism_class.l1 AS organism_class_l1, 
    organism_class.l2 AS organism_class_l2, 
    organism_class.l3 AS organism_class_l3 
FROM activities
LEFT JOIN assays ON assays.assay_id=activities.assay_id 
LEFT JOIN target_dictionary ON assays.tid=target_dictionary.tid 
LEFT JOIN target_components ON target_dictionary.tid=target_components.tid 
LEFT JOIN component_sequences ON target_components.component_id=component_sequences.component_id 
LEFT JOIN component_class ON component_sequences.component_id=component_class.component_id 
LEFT JOIN protein_family_classification ON component_class.protein_class_id=protein_family_classification.protein_class_id 
LEFT JOIN organism_class ON organism_class.tax_id=target_dictionary.tax_id 
LEFT JOIN molecule_dictionary ON activities.molregno=molecule_dictionary.molregno 
LEFT JOIN compound_structures ON activities.molregno=compound_structures.molregno 
LEFT JOIN compound_properties ON activities.molregno=compound_properties.molregno 
WHERE assays.confidence_score > 7 
AND molecule_dictionary.molecule_type='Small molecule' 
-- AND protein_family_classification.l3 like "Peptide receptor (family A GPCR)" #could be "%peptide receptor%" to extract family A and B
AND protein_family_classification.l2 like "Family A G protein-coupled receptor" 
AND organism_class.l2 = 'Mammalia' -- AND target_dictionary.organism = "Homo sapiens" 
-- AND activities.standard_value != '' AND activities.standard_value IS NOT NULL 
AND compound_structures.canonical_smiles IS NOT NULL AND compound_properties.mw_freebase < 1000 
AND ((activities.standard_value <= 1000 AND (activities.standard_type = 'Ki' OR activities.standard_type = 'Kd' OR activities.standard_type = 'IC50' OR activities.standard_type = 'EC50') 
AND activities.standard_units = "nM") OR (activities.standard_value >= 6 
AND (activities.standard_type= 'pKi' OR activities.standard_type = 'pKd' OR activities.standard_type = 'pIC50' OR activities.standard_type = 'pEC50'))) 
AND (assays.assay_type = 'B' OR assays.assay_type = 'F'); 
```

- __NOTE NOTE Very important!!!__ 
>Index the 2 columns(molregno and traget_id) from the custom table filterGpcrMammalia1m that just was created. MySQL workbench is a good start and helpful tool. For this project it is done there.


##**Stages 2 to 5**##
The actions described here will download a set of molecules from the custom ChEMBL table (in the case of this example is called chembl_22). Next will prepare these molecules for Gaston. That is will strip the hydrogen atoms from the molecules and will convert them (the molecules to graphs m2g_rdkit-h.py). Further will engage Gaston in molecules fragmentation and then will convert its output to molecules by applying some filters(g2m_speedUp2.py). In the end further filters are applied(/filter_corect_smiles-part1.py and filter_corect_smiles-part1.py). 


__NOTE:__ An important requirement is that one should have RDKIT installed. Depending on one's installation one should check out the rdkit installation documentation [here ](http://www.rdkit.org/docs/Install.html).

- Download the [needed_scripts_part1](https://bitbucket.org/tsonko/focus-library/src/450bc840e74c?at=master) in your project directory. 
- Navigate to needed_scripts_part1 and find what is the full path to the folder.

        cd $path/needed_scripts_part1
        pwd

- Copy the output from the `pwd` command.
- Unzip the gaston-1.2-re.tar.gz.
- Open `rakefile.rb` in your favorite editor. 
> The *rakefile.rb* contain recipe and dependencies for all of the created files. The file is separated on blocks. In each block is a rule of task. In the rules is line that starts with sh "" this is the command line that one should execute if one wont to generate the ".XXX" file from the rule. Rules can depend to more than one file and therefore there are small functions that solve this problem. 

- Find the lines shown below.


```
#!ruby
SCRIPT_FOLDER = "/media/tsonko/DATA/tt/24k/scripts/git_ready"
NEEDED_DATA =  "/media/tsonko/DATA/tt/needed_data"
GASTON = "/media/tsonko/DATA/tt/24k/scripts/gaston-1.2-re"
```

>- For *SCRIPT_FOLDER = ""* - replace the path in the "" with the path to the need_scrpits_part1 folder (paste the copied pat from point 3).
>- For *GASTON = ""* - replace the path in the "" with the path to the unzipped gaston-1.2-re folder from point 4.
>- For *NEEDED_DATA = ""* - replace the path in the "" with the path to your project directory.
>- Depending on your hardware one can change the available CPUs by finding the line below and change the number accordingly

```
#!ruby
CPU = 14
```
>- Further adjustments in the *Globals* section in the file can be made if needed. 

- Edit all of the places where the database name is mentioned. 
    - Navigate to the `$PATH/needed_scripts_part1/` and execute the following command.

        grep chembl_22 * 

    > The command will provide you with list with all files in the directory where the name of the custom database is mentioned.
    > I did used the db name(chembl_22) as a dbName, user name and password. So when you edit it the password should be entered in places after `-p` flag and the user name after `-u` flag.
    - Find and open the files in your favorite text editor with the database

- Copy the `rakefile.rb` in the project directory - a suggestion would be your project directory to be one up. Furthermore, it would be an advantage if the project directory does not contain anything else than the `needed_scripts_part1` folder.
- Navigate to the project directory and execute the following command.
    
    
        rake chembl_22.translated
        rake chembl_22.filtered_subs1

>**NOTE:** the `chembl_22` before the `.` can be anything,however, the extension (the part after the `.`) HAVE to be exactly the same. In this case I used the database version. However it needs to be the same(xxx.translated and same xxx.filtered_subs1). The output files are text files and can be opened with any text editor. The second command will take significant amount of time and hard drive space (depending on data set more than 24 hours and perhaps more than 80-100 GB). The most time and hdd space consuming step is fragmentation from Gaston.


>**DO NOT CHANGE THEIR NAMES AND/OR EXTENTIONS!!!**.


[point 7 here ](https://www.evernote.com/l/Aexx8dYW-61JcKWA-DdeI7jmiBaLwDumjlA)


##**Stage 6 Fragment characterization ----NEEDS FIX**##

In the Fragment characterization stage we are linking the fragments with the full size parental ligands in order to elucidate the fragment properties and ligand binding preference. This is done by employing the substructure search in InstanJChem (IJC) from ChemAxon(CA). 


- A substructure search is conducted between the fragments and the full size ligands downloaded from ChEMBL(local/custom). This step will allow us to identify the parents of each of the fragments. More over since we already know the accession number of the parental targets(chembl_22.dbSearch_combined file) we can connect them directly to the fragments. At the output of this step we should have file containing the next columns:

     `#SMILES\tID\t#overlaps\taccession_numbers`

One should have InstantJchem installed. An academic license can be obtained from their [web site](https://www.chemaxon.com/)


IJC recognize the input file format based on their extensions and can it work with many different formats. The most interesting for us are the following formats - `.tab`, `.csv` and `.sdf` and `.smi`. One should copy the files we have created and rename them with the proper extensions. In the project directory input the next code: 

    cp chembl_22.dbSearch_combined chembl_22_dbSearch_combined.csv
    cp chembl_22.filtered_subs1 chembl_22_filtered_subs1.smi

They can be generated with the correct names/extentions but it have to be done with involving of regex and I would like to avoid that. 


The tutorial how one should load the files and work with IJC can be found [here](https://www.evernote.com/l/AewzUGCn8TNKDKpqoVUWXMysvtfAbgH1hYA).


The actual substructure search and settings could be found [here](https://www.evernote.com/l/AexUgFKm-sJNfaJm1uwV-rAIE9GtEuGgf2E).


For the purpose of this tutorial I will call the IJC output file `fragment_CA_accessions.tab`. There is a script which would be able to translate the accession numbers but it works with slightly different file format. To fix that one should execute the following command line.  


###Translating ACC IDs for the fragments----NEEDs Fix and more work here ###

Previous step linked the fragments with their parents and respectively to the accession number of the parents targets. In this step we will translate the accession numbers to receptor family information, which is will contribute to the protein classification and selectivity ration calculation later in the workflow. 

##**Stage 7 - Maestro**##

###**Maestro LigPerep+Epik**##

####**Fragment properties calculation**####

The fragments from Gaston and the full size compounds from Enamine data set have to be filtered by some of their physicochemical properties. In order to calculate these properties Scrodinger Maestro is used.

There are several ways to conduct such calculations, however, for easy automation of the workflow a command line method is chosen. There is a significant difference when a command line is used on a local Linux tower or the BR-cluster. The file called `submit.sh` in the repository is used in the `br6` - note of the BR-cluster due to its independence. There might be a problem with the script and the queuing system by not freeing the processes ones the jobs are finished but needs to be tested further. Moreover, the input filename and the number of processes reserved for the jobs need to be changed in the script. As it is now it reserves 64 processes(the hole note). 

One could also use the same command line on a local tower and then has to change the number of use processes accordingly to one's available hardware.  
In any way it is recommendable to create a separate folder inside the project folder where the maestro files will be generated. Maestro and Schrodinger in general are recognizing the files by their extensions. Hence, a copy of the survived fragments with parents `fragment_CA_accessions.tab` is made in the new directory. So from the project folder execute the following command lines. Tip remove the $SCHRODINGER if you run that in green.

Default values for the LigPrep command line. The possible changes can be observed in the Enamine LigPrep code example further below. 

```
#!bash 

cd $PATH/project_folder
mkdir maestro_files
cd maestrofiles
cut -f2,5 ../fragment_CA_accessions.tab| sed 's/,//g' > fragments.smi
$SCHRODINGER/ligprep -i 2 -epik -We,-ph,7.4,-pht,0.0,-ms,1 -ns -r 1 -nt -bff 16 -WAIT -LOCAL -NJOBS 14 -HOST localhost:16 -ismi fragments.smi -omae ligprep_1-out.maegz

```

This is a command line designed for a Linux tower called "green" with 16 available processes. Note that the `NJOBS 14`. It is recommendable to have few less jobs than the available processes. There are house keeping processes that are also running and are not part of the jobs number. So atleast 2 or more processes should be free. 

**NOTE:** The number on `-bff 16` flag should not be changed because that is responsible for assigning the force field that is used in the calculation. In this case - opls3 - which is the newest in Maestro. 

Of-course a Schrodinger Maestro should be installed and licensed in order to perform the needed calculations. 


####**QikProp**####

Ones the ligands are prepared the properties calculation take place in this maestro function/program. In the same directory `$PATH/maestro_files` execute the following command line to start the QikProp calculations.

Default values for the qikprop command line.
`qikprop -nosim -nofast -nosa -outname qikprop_1 ligprep_1-out.maegz`


####**Enamine properties calculation**####

A decision has been taken that Maestro initial input file should be a`.smi` and not a `.sdf` file. The downloaded file form Enamine is in `.sdf` format, thus it is going to be converted to `.smi` for the use of the command line tools. 
One way to do that is by using Open Babel. The hole data set however need to be loaded in IJC for later substructure search there. Once loaded in IJC it easily can be extracted as `.smi`. If needed see how it is done fomr the stage 6 JCM tutorials. 

**NOTE:** The `smiles` format in IJC extract only structure information without identifiers. Thus, it is better to extract it in `.csv` or `.tab` and remove everything from the last step without the structure and idnumber columns. Which essentially is `.smi` format.

Ones formatted to a `.smi` file, the compound properties are calculated in exactly the same way as the *Fragmet_properties* above.

From the main project folder create a separate folder called `enamine`. Copy there the `.smi` file exported from IJC. and apply the steps above:

Possible changes in the LigPrep command line indicated with `<>` sings. 
```
#!bash
mkdir enamine_maestro_files
cd enamine_maestro_files
mv $PATH/<project_filder>/enamine_maestro_files/enmaine.smi .
$SCHRODINGER/ligprep -i 2 -epik -We,-ph,7.4,-pht,0.0,-ms,1 -ns -r 1 -nt -bff 16 -WAIT -LOCAL -NJOBS <nuber of jobs> -HOST localhost:<number_of_available_prcesses> -ismi <input_file>.smi -omae <output_prefix>-out.maegz
```
And later activate the QikProp:

Default values for the qikprop command line:

`qikprop -nosim -nofast -nosa -outname qikprop_1 ligprep_1-out.maegz`

Possible changes inclosed in `<>` signs. 

`qikprop -nosim -nofast -nosa -outname <output_prefix> <lgiprep_input_file>-out.maegz`

###**Maestro Filtering**###

Both the Enamine and the fragment set are filtered according to the filters found in the `Compound_Property_Filters.xlsx` file in the repository. 
The output file from the QikProp is in a `.mae` format, which is a binary (and proprietary),thus it needs to be converted to a `.csv` file so one can read/work with it. Such conversion can be achieved in 2 ways - by using Canvas in GUI(import mae export csv) or employing `$SCHRODINGER/utilities/canvasConvert` command line tool. One can find information about different flags on the command line tool by executing it without any arguments or with `-h` flag.

`/opt/schrodinger2016-3/utilities/canvasConvert -imae /media/tsonko/DATA/tt/22/maestro_prep_and_filtering_for_gaston_output/qikprop_1-out.maegz -ocsv output.csv -u -od ','` 

`/opt/schrodinger2016-3/utilities/canvasConvert -imae <input.mae/maegz file> -ocsv <output.csv file> -u -od ','`

It takes around 15sec for 1000 converting 1000 compounds


Not all of the calculated from QikProp properties are needed. Here is a command line that extract the columns that contain relevant data for the filtering step.

`cut -f1,2,5,6,23,25,26,27,50,59,62,73 -d ',' output.csv | sed 's/,/\t/g' > output_clean.csv` 

or

`cut -f1,2,5,6,23,25,26,27,50,59,62,73 -d ',' <input file>  > <output file>` 

Where `output.csv` is the output from the previous operation and the `output_cleant` is the output file that contain only the properties that are needed.

To apply the filters from the Compound_Property_Filters.xlsx, an integer value is needed in the hydrogen bond donor/acceptor properties. I do not like the way that Maestro handle that property calculation, hence I am  using `cxcalc` command line tool from ChemAxon.

`/home/tsonko/ChemAxon/JChem/bin/cxcalc -h` 

For more information about the command line application execute it with `-h` argument or consult with [this page](https://docs.chemaxon.com/display/docs/cxcalc+calculator+functions#cxcalccalculatorfunctions-hbda). The application works with structural files as `.sdf` and `.smi`. 

**NOTE:** Check the report information on the standart out. Some times `cxcalc` works better with the `.sdf` files instead of `.smi`.

From the `output_clean.csv` above one can easily create `.smi` file by extracting the first two column. So here it is:

`cut -f1,2 output_clean.csv |sed 's/,/\t/g' > output_clean.smi`

Then I in the following command line I combine the `cxcalc` application with some other command line tools to lower the number of needed steps. 

`$PATH/cxcalc hbda -t "acceptorcount,donorcount" -H 7.4 output.smi| perl -lane 'if ($F[0] == "id") {print "$F[1]\t$F[2]\tacc_don_sum"} else {$a = $F[1]+$F[2]; print "$F[1]\t$F[2]\t$a"}' | paste output_clean.csv - | sed 's/,/\t/g' > output_hbda.csv`

The above command line calculate HBDA, their sum and add them to the rest of the properties in the `output_clean.csv` by producing a `output_clean_hbda.csv`, that contains all of the needed properties values for the filtering. 

The `ligfiletr` from Schrodinger does not support anything else then `.mae` files hense I convert them back to that format. 


1.Filter the fragments with the fragment filter properties. 

2.Filter the Enamine full size compounds with 3 different filters .. up to 450 for the focus libraries and 2 other for the diverse library ( if needed calculate the missing properties for full filtering and add them to the `.csv` file before filtering)

3.Conduct *"Similarity search" of similarity matrix* in all vs all fashion for the filtered fragments in canvas. Use MolPrint2D finger print with the 8th Atom type, 64 bit and TC>0.72 cut-off. Extract only the pares with the value above the threshold. Calculate the selectivity ratio based on the fragment parents selectivity information. Keep only the ones with ratio >=4.

1. apply the second similarity measures filters (Heavy atoms, hetero atoms, charge)
2. recalculate the selectivity ratio and apply the >=4 filter.

4.Conduct a substructure search in IJC between the survived fragments (Privileged Substructure - PS) and the filtered Enamine full size compounds (PS as query and Enamine as target). The hits of the query will be used as a pool of compounds for diversity search. 


5.Conduct a similarity search between the PS and filtered Enamine compounds (MolPrint2D AT8, 64bits, TC 0.4)

6.Apply the second similarity filter (totQ and number of heavy and hetero atoms). 

7.Extract the lowest MW hit for each PS. Use as low tc as needed to have one hit for each of PS.

8.Extract the most similar and  hits for each of the PS within the TC >= 0.4. Create a non-redundant list and use it as a seed in the Canvas diversity search. 

8.In Canvas the highest diversity TC is 0.6. For pool of compounds to select from use the hits from substructure search between filtered (300-450)Enamine and PS earlier with the following settings: MolPring2D, AT8, TC=0.6/65,size 3520 - (minus) the number of non-redundant (most similar and with lowest MW).

1. Important use the charged(ligprep-ed) versions of the molecules for the *similarity search* in canvas 
2. Important use the NON-charged versions of the molecules for the *substructure search* in IJC - ChemAxon



[chembl]: ftp://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/releases/ "Optional Title Here"
[22]: ftp://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/releases/chembl_22/chembl_22_schema.png "Optional Title Here"