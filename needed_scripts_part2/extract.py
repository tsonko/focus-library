#!/usr/bin/env python
import sys
import getopt
from sets import Set


def read_ids_for_extraction(idsfile):
    fi = open(idsfile, 'r') 
    ar = Set()
    for line in fi:
        temp = line.rstrip().split()
        ar.add(temp[-1])
    fi.close()
    return ar

def extract_readed_ids(ar,inputFilename, outputFilename):
    fi = open(inputFilename,'r')
    if outputFilename != False:
        fo = open(outputFilename, 'w')
        for line in fi:
            temp = line.rstrip().split()
            if temp[1] in ar:
            #if temp[3] in ar:
                fo.write(line)
    else:
        for line in fi:
            temp = line.rstrip().split()
            if temp[2] in ar:  
            #if temp[3] in ar:
                print line.rstrip() 
 

def usage():
   print "-h\t-help\t\tPrint this message"
   print "-i\t-input\t\tPath to input file"
   print "-d\t-input\t\t Path to the dictionary file"
   print "-o\t-output\t\tPath to output file"
   #print "-c\t-cpu\tNumber of CPUs to use"

def main (argv):
    try:
        opts, args = getopt.getopt(argv, "i:d:o:h", ["input", "query_ids", "output path/filename" "help"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    # default values
    inputFileName = False
    #fileFormat = "smi"
    queryFileName = False
    #aromaticBonds = False
    #aliphaticHeteroAtoms = False
    outputFileName = False
    # arguments
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-i", "--input"):
            inputFileName = arg
	elif opt in ("-d", "--ids"):
	    queryFileName = arg   
        elif opt in ("-o", "--output"):
            outputFileName = arg
        #elif opt in ("-a", "--atoms"):
        #    aromaticAtoms = True
        #elif opt in ("-b", "--bonds"):
        #    aromaticBonds = True
        #elif opt in ("-e", "--hetero"):
        #    aliphaticHeteroAtoms = True
    
    # run
    #ar = dict_query(queryFileName)
    #ar = dict_query3(queryFileName)
    ##ar = dict_query2(queryFileName)
    #extract3(ar, inputFileName, outputFileName)
    ##extract(ar, inputFileName, outputFileName)
    #convertStructures(inputFileName, fileFormat, aromaticAtoms, aromaticBonds, aliphaticHeteroAtoms)
    
    ar = read_ids_for_extraction(queryFileName) 
    extract_readed_ids(ar,inputFileName, outputFileName)

if __name__ == "__main__":
    main(sys.argv[1:])
