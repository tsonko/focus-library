#!/usr/bin/env python
import sys
from collections import defaultdict
from collections import OrderedDict
from sets import Set

#This script require a file output from index2id_new.py which have been sorted with the
#This script also require a file output from translate_combined_unique.py

##usage: command line
##id2structure+hits.py qvsnonpep_ecfp4_0.3.in2id bbb+properties.smi nonpep.translatedNonPep NonPep_Binding
##>$PATH/in2structire.py query+properties.sim data+properties.sim xxx.in2id pep/nonpep
##assert len(sys.argv) == 4 'usage: %s query.id, data.id, *.sim \n' % sys.argv[0]


def read_in2id (filename):
    d = defaultdict(list)
    fh_IDs = open(filename, 'r') 
    
    dataIDs = Set()
    qIDs = Set()
    #maindict = defaultdict(lambda:defaultdict(list))
    #dictoflist = defaultdict(list)
    dictoflist = defaultdict(list)
    ar =[]
    test = ''
    for line in fh_IDs:
	if line[0] != '#':
            qID, dataID, TC = line.rstrip('\n').split('\t')
            #dataID, qID, TC = line.rstrip('\n').split('\t')
            qIDs.add(qID)
	    #maindict[qID][dataID] = float(TC)
            dictoflist[qID].append([dataID, TC])
            #dictoflist[qID].append(dataID)
            dataIDs.add(dataID)
   # for i in dictoflist.keys():
   #     for l in dictoflist[i] :
   #         print i,l
    fh_IDs.close()
    #print dictoflist 
    return qIDs, dataIDs, dictoflist

def read_file (filename) :
    fh = open(filename, 'r')  
    data_dict = defaultdict(list)
    
    for line in fh:
        if line[0] != "#":
            smi, name, restLine = line.rstrip('\n').split('\t', 2)
#        #restLine = [smi] + restLine.split('\t')
	data_dict[name]= line.rstrip('\n').split('\t')
 
    fh.close()
    return data_dict 


def formating(datadict,connections):
    
    #for i in connections['1661184']:
    #    print datadict[i[0]][1:8]
    #stop
#    index = 1

    #loop over the qids
    for k in connections.keys(): # k in connections.keys() is a query_ID
    #for k in connections.keys():
	#if k[0:2] != "ID":
	familiesN = Set()         # set containing nonpeptide bindig familises Unique
	familiesP = Set()         # set containing peptide bindig familises Unique
        #families = 0
	dataList=[]
        TC = []
	
	#qdata string 
	temp_list = datadict[k][:5]
        temp_list.insert(2, temp_list[1])
        #qdata = '\t'.join (qdict[k]) #taking the informationfor the query molecule
        qdata = '\t'.join (temp_list) #taking the informationfor the query molecule
	#loop over the query hits
	for x in connections[k]:
	    dataString_temp = []
            TC.append(x[1])
#            #families = families + int(datadict[x[0]][3])
#
#unique families count		
	    temp_familiesN = datadict[x[0]][-1].split(',')
            for familyN in temp_familiesN:
                if familyN != 'None':
                #if familyN != '':
                     familiesN.add(familyN)
	    temp_familiesP = datadict[x[0]][-2].split(',')
            for familyP in temp_familiesP:
                if familyP != 'None':
                #if familyP != '':
                     familiesP.add(familyP)
	    
            dataString_temp= datadict[x[0]][:5]
            #dataString_temp= datadict[x[0]] # test if the output is correct
	    dataString_temp.insert(2,k)
	    dataString_temp.append(x[1])
	    dataList.append(dataString_temp)
            ratio = 0.
            if len(familiesN) == 0:
                ratio = len(familiesP)/1.
            else:
                ratio = float(len(familiesP))/len(familiesN)
#	print qdata+"\t"+str(len(familiesP))+'\t'+','.join(familiesP)+"\t"+str(len(familiesN))+'\t'+','.join(familiesN)#+"\t"+str(max(TC))  #test for the correct output
	print qdata+"\t"+str(len(familiesP))+"\t"+str(len(familiesN))+'\t'+str(ratio)#+"\t"+str(max(TC))
	dataList = sorted(dataList, key=lambda x: x[-1], reverse=True)
	for l in dataList:
	    print "\t".join(l[:-1])+"\t\t\t"+str(l[-1])

def main(argv):
    
    qids,dataids, connections = read_in2id(sys.argv[1]) #xxx.in2id file comming rom in2id_new.py
    datadict = read_file(sys.argv[2])  # data+query_properties.smi file
    
#    indaicator = sys.argv[4]
    #print "#index\tSMILES\tname\tnumpep_families\tnumNonpep_families\tratio\tnum_NonpepFamiliesVS\tsubStruct_ID\tcompound_ID\tn\treceptor_families_names\tTC\tpep/nonpep"
    print "#SMILES\tTarget_ID\tQuery_ID\tnum_pep_families\tnum_NONpep_families\tratio_Pep/Nonpep_Families\tnum_pepFamilies_SS\tnum_NONpep_families_SS\tTC"
    formating(datadict, connections)

if __name__ == '__main__':
    main(sys.argv[1:])
