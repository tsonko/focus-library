#!/usr/bin/python

import getopt
from sets import Set
import sys

def dict_read(dictFileName):

    fh = open(dictFileName, 'r')
    lookup = dict()
    for line in fh:
        temp = line.rstrip('\n').split('\t')

        if temp[0] not in lookup:
#            lookup[temp[0]] = [temp[1:]]
            lookup[temp[0]] = [temp[-3], temp[-2], temp[-1]]
        
    return lookup

def translate(inputFileName, lookup_dict):
    #print header
    print 'SMILES\tName\t#PepFamilies\t#NonPepFamilies\tratio\tPep_Targets\tPep_Families\tNonPep_Targets\tNonPep_Families'
    fh2=open(inputFileName, 'r')
#    header = fh2.readline()
    
    #index = -1
    for line in fh2:
        print line
        targetFamiliesP = Set()
        targetsP = Set()
        targetFamiliesN = Set()
        targetsN = Set()
        #exceptions = ['P13500','P13501']
        exceptions = [ "B6F061","G3H407","G3I1B6","G7Q3P2","O70424","P13500","P13501","P35365","P79291","P79399","P79400","P97266","Q2NNR4","Q2XUH7","Q4VQ11","Q6Q253","Q8MHZ5","Q8MI13","Q8SPZ1","Q8VH25","Q8VH26","Q8WMX0","Q923X8","Q9ERT1","Q9R1L2"]
        
        a = -1
        if line.count(';') !=0: 
            temp  = line.rstrip('\n').split(';')
        #elif line.count('\t') !=0:
        #    temp = line.rstrip('\n').split('\t') 
        else:
            temp = line.rstrip('\n').split()
        
        #smile = temp[0]
        smile = temp[1]
        accessions = temp[-1].split(',')
        #index=temp[1]
        index=temp[3]
        #index = index+1
        chembl_id = temp[2]
        for i in accessions:
            if i not in exceptions:
                if i in lookup_dict:
                    if lookup_dict[i][-1] == "N":
                        targetsN.add(lookup_dict[i][1])
                        targetFamiliesN.add(lookup_dict[i][0])
                    elif lookup_dict[i][-1] == 'P':
                        targetsP.add(lookup_dict[i][1])
                        targetFamiliesP.add(lookup_dict[i][0])
                else:
                    raise NameError('%s is not in the dictionary' %i)
#        print smile+'\t'+'ID'+str(index)+'\t'+str(len(targets))+'\t'+str(len(targetFamilies))+'\t'+str(index)+'\t'+','.join(targetFamilies)+'\t'+ ','.join(targets)
#        print smile+'\t'+'ID'+str(index)+'\t'+chembl_id+'\t'+str(len(targetFamilies))+'\t'+str(len(targets))+'\t'+str(index)+'\t'+','.join(targetFamilies)+'\t'+ ','.join(targets)
        ratio = float(0.)
        if len(targetFamiliesN) == 0:
            ratio = float(len(targetFamiliesP))/-1
        else:
            ratio = float(len(targetFamiliesP))/len(targetFamiliesN)
        

        #if ratio != False:
#        if ratio>3 or ratio < -3 :
        #if ratio>4 :
        #if ratio< -4:
#            print smile+'\t'+str(index)+'\t'+str(len(targetFamiliesP))+'\t'+str(len(targetFamiliesN))+'\t'+str(ratio)        #without "ID" in front of the fragments identifyer 
                #print smile+'\t'+'ID'+str(index)+'\t'+str(len(targetFamiliesP))+'\t'+str(len(targetFamiliesN))+'\t'+str(ratio)  #with "ID" in front of the fragments idetifier 
        #else:
            #print smile+'\t'+str(index)+'\t'+str(len(targetFamiliesP))+'\t'+str(len(targetFamiliesN))+'\t'+str(ratio)           #without "ID" in front of the fragments identifyer 
            #print smile+'\t'+'ID'+str(index)+'\t'+str(len(targetFamiliesP))+'\t'+str(len(targetFamiliesN))+'\t'+str(ratio)     #with "ID" in front of the fragments identifyer 
        if len(targetFamiliesP) == 0 :
            print smile+'\t'+str(index)+'\t'+str(len(targetFamiliesP))+'\t'+str(len(targetFamiliesN))+'\t'+str(ratio)+'\t'+'None'+'\t'+'None'+'\t'+','.join(targetsN)+'\t'+','.join(targetFamiliesN)        
#            print smile+'\t'+str(index)+'\t'+str(len(targetFamiliesP))+'\t'+str(len(targetFamiliesN))+'\t'+str(ratio)+'\t'+'None'+'\t'+','.join(targetFamiliesN)        #without "ID" in front of the fragments identifyer 
        elif len(targetFamiliesN) == 0 :
            print smile+'\t'+str(index)+'\t'+str(len(targetFamiliesP))+'\t'+str(len(targetFamiliesN))+'\t'+str(ratio)+'\t'+','.join(targetsP)+'\t'+','.join(targetFamiliesP)+'\t'+'None'+'\t'+'None'
#            print smile+'\t'+str(index)+'\t'+str(len(targetFamiliesP))+'\t'+str(len(targetFamiliesN))+'\t'+str(ratio)+'\t'+','.join(targetFamiliesP)+'\t'+'None'        #without "ID" in front of the fragments identifyer 
        elif len(targetFamiliesP) !=0 and len(targetFamiliesN) != 0 :
            print smile+'\t'+str(index)+'\t'+str(len(targetFamiliesP))+'\t'+str(len(targetFamiliesN))+'\t'+str(ratio)+'\t'+','.join(targetsP)+'\t'+','.join(targetFamiliesP)+'\t'+','.join(targetsN)+'\t'+','.join(targetFamiliesN)        #without "ID" in front of the fragments identifyer 

        
# help message
def usage():
    print "-h\t-help\t\tPrint this message"
    print "-i\t-input\t\tPath to input file"
    print "-d\t-input\t\tPath to the dictionary file"
    print "r\t-input\t\tCutoff ratio(4)"
    #print "-o\t-output\t\tPath to output file"
    #print "-c\t-cpu\tNumber of CPUs to use"

def main (argv):
    try:
        #opts, args = getopt.getopt(argv, "i:d:o:c:h", ["input", "dictionary", "output", "cpu", "help"])
        opts, args = getopt.getopt(argv, "i:d:r:h", ["input", "dictionary", "ratio", "help"])

    except getopt.GetoptError:
        usage()
        sys.exit(2)

    # default values-
    inputFileName = False
    outputFileName = False
    ratio = False
    #cpus = 1
    
    # arguments
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-i", "--input"):
            inputFileName = arg
        elif opt in ("-d", "--dictionary"):
            dictFileName = arg
        elif opt in ("-r", "--ratio"):
            ratio = arg
        #elif opt in ("-o", "--output"):
        #    outputFileName = arg
    lookup_dict = dict_read(dictFileName)
    
    translate(inputFileName, lookup_dict)
    
if __name__ == "__main__":
    main(sys.argv[1:])
