import sys
from sets import Set

def main(argv):

    s = set()
    #f = open ('4k_sim_matrix.csv')
#    print argv
    f = open (argv[0])
    header = f.readline().rstrip().split(',')
    sys.stdout.write('#{0},{1},{2}\n'.format('mol 1', 'mol 2', 'tanimoto'))
    index = 0
    for line in f:
        tabs = line.rstrip().split(',')
#        print tabs
        id1 = tabs[0]
        for i, tanimoto in enumerate(tabs[1:], 1):
            id2 = header[i]
#            if id1 != id2 and float(tanimoto)>=0.72:
            if float(tanimoto)>=0.35:
                #sys.stdout.write('{0},{1},{2}\n'.format(id2, id1, tanimoto))
                print '{0},{1},{2}'.format(id2, id1, tanimoto)
        index = index+1
    f.close()

#canvas,Z46161759,Z46162360,Z46161781
#Z46161759,1,0.171429,0.333333
#Z46162360,0.171429,1,0.102564
#Z46161781,0.333333,0.102564,1



if __name__ == '__main__':
    main(sys.argv[1:])
