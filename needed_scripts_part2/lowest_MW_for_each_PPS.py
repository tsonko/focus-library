import csv

enamine_id_MW = {}
final_dict = {}
enamine_dict={}
#load the fullsize desalt compounds with correct MW
with open('enamien_hits_filtered_notcharged.csv',  'r') as fii1:
    
    for line in csv.reader(fii1, delimiter=','):#,skipinitialspace = True):
        if 'MW' not in line:
            enamine_id_MW[line[1]] = float(line[-1])
            enamine_dict [line[1]] = line
##read the conection table and pwrkit from there 
with open('pps_hhc_xls_notcharged_en_hits.csv', 'r') as fii2:
    header = fii2.readline()
    #for each line
    for line2 in csv.reader(fii2, delimiter=','):
        pps_id = line2[4]
        #print line2
        hit_ids = line2[-1].split(',')
        test_MW = 600.
        #for each hit in the line
        for i in hit_ids:
            #print i        
            print pps_id, i,enamine_id_MW[i]
            #compare theirs MW and if it is lower than the one existed writed down in the final_dict dictionary
            if enamine_id_MW[i] >=300 and enamine_id_MW[i] <=450 and enamine_id_MW[i] < test_MW :
                test_MW = enamine_id_MW[i]
                final_dict[pps_id]=i
#writed in a file                
#open the file for writing 
with open ('lowset_hit_for_ppsi10.csv', 'w' ) as foo:
    for k in final_dict:
        ##for each of the keys in the dict print the enamine-smile\tenamine_id\tpps\tMW
        #print "%s\t%s\t%s\t%s\n" %(enamine_dict[final_dict[k]][0],final_dict[k],k, enamine_dict[final_dict[k]][2])
        foo.write( "%s\t%s\t%s\t%s\n" %(enamine_dict[final_dict[k]][0],final_dict[k],k, enamine_dict[final_dict[k]][3]))
#print len(final_dict)
