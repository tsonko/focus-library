from rdkit import Chem

#def read_smi(inputfile):
#with open('MP_AT_NS_corr_tc06_id2str.csv', 'r') as f:
    
def carbon_hetero(smi):
    
 #   smi = 'CCNCN(C)C1CCN(Cc2ccccc2)CC1'
    mol = Chem.MolFromSmiles(smi, sanitize = False)
    carbons = 0
    heteroatoms = 0
    numatoms = mol.GetNumAtoms()
    for num, atom in enumerate(mol.GetAtoms()):
        anum = atom.GetAtomicNum()
        if anum == 6 :
            carbons += 1
        elif anum !=6 and anum != 1:
            heteroatoms += 1    

    #print carbons 
    #print heteroatoms
    #print numatoms
    return numatoms,heteroatoms
    #atoms.append("v %s %s" %(num,atom.GetAtomicNum()))

#with open('11', 'r') as f:
#with open('/media/tsonko/DATA/tt/chembl21/mac/chembl21_mammalia/2nd_similarity_deff/2pps_2nddeff.csv', 'r') as f:
#with open('totQ-for_similarity_search1.tab', 'r') as f:
with open('MP_AT8_qVSq_incl_0.72_totQ.csv', 'r') as f:
#with open('100', 'r') as f: #test
    header = f.readline()
    print header.rstrip()
    flag = 0
    pps_carbons = 0 
    pps_hetero = 0
    pps_charge = 0 
    
    for line in f:
        temp=line.rstrip().split()
        temp[1],temp[2] = temp[2], temp[1]
        carbons,hetero = carbon_hetero(temp[0])
        temp.append(carbons)
        temp.append(hetero)
    

        if len(temp)==12 and temp.count('') <1:
            
            pps_carbons = carbons
            pps_hetero = hetero
            pps_charge = temp[-3]
            flag +=1
#            print flag,temp
            temp = map(str, temp)
            print '\t'.join(temp)
           
        #else:    
        elif len(temp) < 11:
        #elif temp.count('') != 0 :
            target_carbotns = carbons
            target_hetero = hetero
            target_charge = temp[-3]
            flag += 1
#            print flag, temp
#            print "pps target\t",pps_charge,target_charge    
            if pps_charge == target_charge:
                #flag +=1
                if pps_carbons >= target_carbotns:
                    a = pps_carbons 
                    b = target_carbotns
                elif pps_carbons < target_carbotns:
                    b = pps_carbons 
                    a = target_carbotns

                if pps_hetero >= target_hetero :
                    c = pps_hetero
                    d = target_hetero
                elif pps_hetero < target_hetero :
                    d = pps_hetero
                    c = target_hetero
                if  a - b <= 4 and c - d <= 2: #correct
                #if pps_carbons - target_carbotns <= 4 and pps_hetero - target_hetero <= 2: #correct
                #if pps_carbons - target_carbotns > 4 and pps_hetero - target_hetero > 2 : #wrong
#                    #flag +=1
#                    print carbons,hetero
                    temp.insert(6,'none')
#                    temp.insert(6,'none')
                    temp = map(str, temp)
                    print '\t'.join(temp)
#                    print flag,temp
f.closed
