from collections import defaultdict

pps_dict = defaultdict(list)
header = ''
data_dict = defaultdict()
#with open('10.csv', 'r') as fii:
#with open('pps_vs_enamine.in2id', 'r') as fii:
with open('/media/tsonko/DATA/tt/22/protein_receptors/sim_search_pps/protein_ligands_04.id2id', 'r') as fii:
    #temp = csv.reader(fii, delimiter='\t')#, quotechar='|')
#    if line[0] == '#':
#            header = fii.readline()
    for line in fii:
        if line[0] != '#':
            temp = line.rstrip().split()
            pps = temp[0]
            enamine_hit = temp[1]
            tc = float(temp[2])
            if tc >=0.5:
                if pps not in pps_dict:
                    pps_dict[pps].append( [enamine_hit,tc])
                else:
                    if tc > pps_dict[pps][-1][-1]:
                        #pps_dict[pps] = [enamine_hit,tc]
                        pps_dict[pps] = [[enamine_hit,tc]]
                        #pps_dict[pps][-1] = [enamine_hit,tc]
                    elif tc == pps_dict[pps][-1][-1]:
                        pps_dict[pps].append([enamine_hit,tc]) 
            
fii.close()

with open('enamine_target_id_mw', 'r') as fii2:
    for line in fii2: 
        temp=line.rstrip().split(',')
        data_dict[temp[0]] = temp[1]
fii2.close()




#print str(27493142),pps_dict['27493142'][0][0]
#for i,n in enumerate (pps_dict['27493142']):
#    print pps_dict['27493142'][i][0]
for i in pps_dict:
    l = len(pps_dict[i])
    if l >=1:    
        mw_test = [] 
        for n,hit in enumerate(pps_dict[i]):
            en_hit_name =  pps_dict[i][n][0]
            mw = data_dict[en_hit_name] 
            tc = pps_dict[i][n][1]

            if n == 0 :
                mw_test = [en_hit_name,mw,tc]
            elif n > 0:
                #print mw_test[-1],data_dict[en_hit_name]
                if data_dict[en_hit_name] < mw_test[-1]:
                    mw_test = [en_hit_name, data_dict[en_hit_name,tc]]
                #elif data_dict[en_hit_name] == mw_test[-1]:
                #    print 1
        print i+'\t'+'\t'.join(map(str,mw_test))
#    elif l == 1:
#        print 'adfa\t'+pps_dict[i][0][0]

#print i,pps_dict['27493142']
