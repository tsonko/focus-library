#!/usr/bin/env python3
import sys
#from urllib2 import urlopen
import requests
import json
from collections import OrderedDict
#from sets import Set
import csv

def find_cid(molecule_chembl_id):
    cid = set()
    temp= []
    url = 'https://www.ebi.ac.uk/unichem/rest/src_compound_id_all/'+molecule_chembl_id+'/1/22'
    response = requests.get(url)
    lig_data= response.json()
    for i,x in enumerate(lig_data):
        temp.append(lig_data[i]['src_compound_id'])
    if len(temp) >1:
        cid = ';'.join(temp)
    elif len(temp)==1:
        cid = temp[0]
        
#    print (cid)
    return cid

def extract_ligands(chemid, header, offset):
    #print (chemid)
    header_test = set()
#    print (len(header))
    #print offset
    url = 'https://www.ebi.ac.uk/chembl/api/data/activity.json?target_chembl_id='+chemid+'&limit=10000&offset='+str(offset)
    #print url #test
    response = requests.get(url) 
    ligand_data = response.json()
    #print (ligand_data)
    count = int(ligand_data['page_meta']['total_count'])
    
    #tesing the header size 
    for x,ligand in enumerate(ligand_data['activities']):
        #print ligand 
        for key in ligand_data['activities'][x].keys():
            header_test.add(key)
        #print (len(header_test))
        test = set(header) | set(header_test)
#        print (len(test))
        #test.add('adfa') #testing the error message 
        if len(test)!=32:
            #print(test-set(header))
            raise NameError('unknown column in the list of columns\n')
   
    return ligand_data['activities'], count
 

#function on python2
#def extract_ligands(chemid, header, offset):
#    header_test = Set()
#    #print offset
#    url = 'https://www.ebi.ac.uk/chembl/api/data/activity.json?target_chembl_id='+chemid+'&limit=10000&offset='+str(offset)
#    #print url #test
#    response = urlopen(url)
#    ligand_data = json.loads(response.read())
#    count = int(ligand_data['page_meta']['total_count'])
#    
#    #tesing the header size 
#    for x,ligand in enumerate(ligand_data['activities']):
#        #print ligand 
#        for key in ligand_data['activities'][x].keys():
#            header_test.add(key)
#        test = set(header) | set(header_test)
#        #test.add('adfa') #testing the error message 
#        if len(test)!=31:
#            raise NameError('unknown column in the list of columns\n')
#   
#    return ligand_data['activities'], count
    

def define_header(chemid):
    header_line = set()
    url = 'https://www.ebi.ac.uk/chembl/api/data/activity.json?target_chembl_id='+chemid
    #url = 'http://gpcrdb.org/services/protein/adrb2_human/'
    response = requests.get(url) 
    ligand_data = response.json()
    for i in ligand_data['activities'][0].keys():
        header_line.add(i)

    #print ligand_data['activities'][0]
    return header_line
    
def read_input_file(filename):
#    print (filename)
    numbers=[]
    with open(filename, 'r') as fii:
        for line in fii:
            if line != '\n':
            #print line
                temp = line.rstrip().split()
                numbers.append(temp[-1])
    #print numbers[0], len(numbers)
    return numbers
#            get_chembl_id(temp[-1]


def main(argv):
    input_filename = argv[0]
    #reads the ids generated from the previous scpript.
    #chembl_numbers = read_input_file('uniprot_chembl.csv')
    chembl_numbers = read_input_file(input_filename)
    #print (chembl_numbers)
    
    #takes the first ligand for the first gpcr from the list above
    #and creates a template for columns in the csv as well as it will 
    #be used as test later for consistency in the column arangements
    header = define_header(chembl_numbers[0])
    #with open('test2.1.csv', 'w') as output_file:
    with open(argv[1], 'w') as output_file:
       
        #dict_writer = csv.DictWriter(output_file, new_header, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        dict_writer = csv.DictWriter(output_file, header, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        dict_writer.writeheader()
        #dict_writer.writerows(toCSV)
        count = int
        for num in chembl_numbers:
               
        #for num in chembl_numbers[0:2]:        
            print (num)
            offset = 0
            d,count = extract_ligands(num, header, offset)
            #print (d)
#            for m,x  in enumerate(d):
#                cid = find_cid(d[m]['molecule_chembl_id'])
#                d[m]['cid'] = cid
##                print (d[m])
            dict_writer.writerows(d)

#            for k in d[0].keys():
#                print (k, d[0][k]) 
            if count > 1000:
    #testing and handeling more than 1000 compounds
                iterations = int((count/1000.)+0.9999)
                for i in range(1,iterations):
                    offset = offset+1000
                    d,count2 = extract_ligands(num, header, offset)
#                    for m,x  in enumerate(d):
#                        cid = find_cid(d[m]['molecule_chembl_id'])
#                        d[m]['cid'] = cid
                    dict_writer.writerows(d)
            
if __name__ in "__main__":
    main(sys.argv[1:])
    #main()
