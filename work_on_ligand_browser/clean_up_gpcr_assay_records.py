#!/usr/bin/env python3

# -*- coding: uft-8 -*-
import sys
import csv
import requests
import pandas as pd 
from collections import Counter
import xlsxwriter


#this function does not work as intended
#def check_for_peptide(chembl_mol_ids, foo_pep_sm):
#    print ('check peptideds')
#    with open(foo_pep_sm, 'w') as foo:
#        writer = csv.writer(foo)
#        pep_sm={}
#        for chembl_mol_id in chembl_mol_ids:
#            url = 'https://www.ebi.ac.uk/chembl/api/data/molecule/{}.json'.format(chembl_mol_id)
#            response = requests.get(url)
#            data = response.json()
#            
#            if data['helm_notation'] == None:
#                writer.writerow([chembl_mol_id, 'sm'])
#                pep_sm[chembl_mol_id] = 'sm'
#            elif 'PEPTIDE' in data['helm_notation']: 
#                writer.writerow([chembl_mol_id, 'peptide'])
#                pep_sm[chembl_mol_id] = 'peptide'
#    #    for key, value in pep_sm():
#    #        writer.writerow([key, value])
                
    



def readfii(fii):
    print (fii)
    df = pd.read_csv(open(fii,'rb'),low_memory = False)
    return df

def clean_up_df(df):
    df = df[df['assay_type'] != 'U']
    df = df[df['assay_type'] != 'A']
    df = df[df['data_validity_comment'].isnull()]
    df = df[df['activity_comment'] != 'inconclusive' ]
    df = df[df['activity_comment'] != 'Inconclusive' ]
    df = df[df['pchembl_value'].notnull()]
    df = df.loc[(df['standard_units'] == 'nM') | (df['standard_units'].isnull()) | (df['standard_units']== 'um') |
        (df['standard_units'] == 'M') | (df['standard_units'] == 'pmol') | (df['standard_units'] == 'mM') | 
        (df['standard_units'] == 'fmol') | (df['standard_units'] == 'pM') | (df['standard_units'] == 'nmol') | 
        (df['standard_units'] == 'fM')]
#    df = df[df['standard_relation'].notnull()]
    df = df[df['standard_value'].notnull()]
    cols = df.columns.tolist()
    cols = sorted(cols)
    df = df[cols]
    return df


def save_in_file(foo,df):

    with open(foo, 'w') as output_file:
        #dict_writer = csv.DictWriter(output_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        df.to_csv(path_or_buf=output_file, quoting = csv.QUOTE_ALL, quotechar='"' )

def main (argv):
    #fii = 'input0_head1k.csv'
    #fii = 'input0.csv'
    #foo = 'new_input3.csv'
    #foo_pep_sm = 'pep_sm.txt'
    fii = argv[0]
    foo = argv[1]
    #foo_pep_sm = argv[2]
    print (fii, foo)#,foo_pep_sm)
    # reading and cleanning the data
    df = readfii(fii)
    df = clean_up_df(df)
############test test #################
    #chembl_mol_ids =[] #set()
    l = df['activity_id'].tolist()
    n = Counter(l)
    for key, value in n.items():
        if value >1:
            print (key)
############ test end ###############
    #check_for_peptide(chembl_mol_ids,foo_pep_sm)  #this one does not work as intended 


    save_in_file(foo,df)

if __name__ == '__main__':
    main(sys.argv[1:])




############from main begin#########################
##    df = df.loc[(df['standard_type'] == '-Log Kdiss')|(df['standard_type'] =='-Log KD50') |(df['standard_type'] == 'pKd')| (df['standard_type'] == 'pKD')|(df['standard_type'] == 'logKd')| (df['standard_type'] == '-Log Kd')|(df['standard_type'] == 'Log Kd')|(df['standard_type'] == '-Log KD')|(df['standard_type'] == 'Log KD') ]
#    
#    #creatin of the convertion dictionarys 
#    kis = ['Ki', 'Adjusted Ki', 'ki', 'Ki app (inact)', 'Ki app', 'Ki(app)', 'Ki_app', "Ki'", 'Ki"', "KI'","K'i",
#        'Kiact', 'Ki high', 'Ki low', 'KiH', 'KiL', 'Kii', 'KII', 'Kic', 'Ki.c', 'Ki comp', "Ki' uncomp"]
#    kds = ['Kd', 'Dissociation constant', 'K app', 'K Bind', 'K calc', "Kd'", 'KD app', "KD'", 'Kd(app)', 'KD50', 'Kdiss', 
#        'Relative Kd', 'Binding constant', 'K aff', 'K diss', 'KD/Ki']
#    ic50s = ['IC50', 'IC50 app', 'IC50 max', 'I50', 'Mean IC50', 'IC50H', 'IC50L']
#    ec50s = ['EC50']
#    ed50s = ['ED50']
#    kbs = ['Kb','KB', 'Kbapp']
#    id50s = ['ID50']
#    other = ['Potency']#, 'Activity','Inhibition']
#    a2s = ['A2']
#    std_lists = [kis, kds, ic50s, ec50s, ed50s, kbs, id50s,a2s,other]
#    
##    df = df[df['published_type'] != df['standard_type']]
##    df = df[df['standard_type']=='pKd']
#   
#    for std_list in std_lists:
#        df = gpcr_columns(df,std_list)
#
#    pkds = ['Kd', '-Log Kdiss', '-Log KD50', 'pKd', 'pKD', 'logKd', '-Log Kd', 'Log Kd', '-Log KD', 'Log KD']
#
#    p_lists = [pkds]
#    for p_list in p_lists:
#        print p_list
#        df = gpcr_convert_P(df,p_list)
#    
#    df = df[df['standard_type'] != df['gpcr_standard_type']]
#    df = df[df['standard_type'] == 'logKd']


###### remove the fixed standard_types 
##    for std_list in std_lists:
#        df = remove(df,std_list)

#    l = df['published_relation'].tolist() 
#    m = df['target_chembl_id'].tolist() 
#    su = zip(l,m)
#    l = df['standard_relations'].tolist() 
#    c = Counter(l)
#    print c
#    c = Counter(su)
#   for i in c.keys():
#        print '%s\t%d' %('\t'.join(i),c[i])
#        print '%s\t%d' %(i,c[i])
#

############from main END ########################



#def remove (df,std_list):
#    for std_type in std_list:
#        df=df[df['standard_type']!=std_type]
#
#    return df
#
#
#def gpcr_columns (df,std_list):
#    
#    for std_type in std_list:
#        #print std_type
#        #dealing with the type 
#        df.loc[(df['standard_type'] == std_type), 'gpcr_standard_type'] = std_list[0]
#        #the problem with not correct transfomation between pA2 and Kd
#        df.loc[(df['standard_type'] == 'Kd') & (df['published_type'] == 'pA2') | 
#                (df['published_type'] == 'pA2 app'), 'gpcr_standard_type'] = 'A2'
#        
##        df.loc[(df['standard_type'] == 'Potency') & (df['standard_value'].notnull()) , 'gpcr_standard_type'] = 'Potency x50'
##        df.loc[(df['standard_type'] == 'Potency') & (df['standard_value'].isnull()) , 'gpcr_standard_type'] = 'Potency Kx50'
##        df.loc[(df['standard_type'] == 'Activity') & (df['standard_value'].notnull()) , 'gpcr_standard_type'] = 'Activity x50'
##        df.loc[(df['standard_type'] == 'Activity') & (df['standard_value'].isnull()) , 'gpcr_standard_type'] = 'Activity Kx50'
##        df.loc[(df['standard_type'] == 'Inhibition') & (df['standard_value'].notnull()) , 'gpcr_standard_type'] = 'IC50'
##        df.loc[(df['standard_type'] == 'Inhibition') & (df['standard_value'].isnull()) , 'gpcr_standard_type'] = 'Ki50'
#
#
#
#        #dealing with the relation (>,=,>=, None)
#        df.loc[(df['standard_type'] == std_type) & (df['standard_relation'] == '=')|
#            (df['standard_relation'] == '>') | (df['standard_relation'] == '>='), 'gpcr_standard_relation'] = df['standard_relation']
#            #translate epty relation with values to equall '='
#        df.loc[(df['standard_type'] == std_type) & (df['standard_relation'].isnull()) & 
#                (df['standard_value'].notnull()), 'gpcr_standard_relation'] == '=' 
#            #translate '<' in standard relation to > in gpcr_standard_relation. 
#            #there are no Logs or Ps in the cases passed to this def therefore we do not need to check for these. 
#        df.loc[(df['standard_type'] == std_type) & (df['standard_relation'] == '<'), 'gpcr_standard_relation'] = '>'
#        #dealing with units and values
#        df.loc[(df['standard_type'] == std_type) & (df['standard_units'] == 'nM') | (df['standard_units'] == 'nmol') | 
#                (df['standard_units'] == 'nm'), 'gpcr_standard_value'] = df['standard_value']
#        df.loc[(df['standard_type'] == std_type) & (df['standard_units'] == 'nM') | (df['standard_units'] == 'nmol') | 
#                (df['standard_units'] == 'nm'), 'gpcr_standard_units'] = 'nM' 
#
#    return df
#
#def gpcr_convert_P (df,std_list):
#    for p_type in std_list[1:]:
#        print p_type
#        df.loc[(df['standard_type'] == p_type), 'gpcr_standard_type'] = std_list[0]
#        if p_type[0] == '-' or p_type[0] == 'p':
#
#            df.loc[(df['standard_type'] == p_type) & (df['standard_value'].notnull() ), 'gpcr_standard_value'] = 10**(df['standard_value']*-1)*10**9
#        elif p_type[0] == 'l' or p_type[0] == 'L':
#            df.loc[(df['standard_type'] == p_type) & (df['standard_value'].notnull() ), 'gpcr_standard_value'] = 10**(df['standard_value']*-1)*10**9
#            df.loc[(df['standard_type'] == p_type) & (df['standard_relation'] == '=') | (df['standard_relation'] == '>') | 
#                    (df['standard_relation'] == '>=') , 'gpcr_standard_relation'] = df['standard_relation']
#            df.loc[(df['standard_type'] == p_type) & (df['standard_relation'] == '<'), 'gpcr_standard_relation'] = '<'
#            df.loc[(df['standard_type'] == p_type) & (df['gpcr_standard_value'].notnull()), 'gpcr_standard_units'] = 'nM' 
#
#    return df
#
#
#
##notes usefull code
#
##df = df[df['data_validity_comment']!= 'No data provided for value, units or activity_comment, needs further investigation'] 
##df = df[df['data_validity_comment']!= 'Data have been checked against the publication and are believed to be accurate']
##df = df[df['data_validity_comment']!= 'Data have been checked against the publication and are as reported - possibly an error was made by the author']
##df = df[df['data_validity_comment']!= 'Units for this activity type are unusual and may be incorrect (or the standard_type may be incorrect)']
##df = df[df['data_validity_comment']!= 'Units for this activity type are unusual and may be incorrect (or the standard_type may be incorrect)']
