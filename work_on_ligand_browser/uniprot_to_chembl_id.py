#!/usr/bin/env python3
import sys
import requests #python 3
#from urllib2 import urlopen #python 2
import json
from collections import OrderedDict
#from sets import Set #python 2


    

def get_chembl_id(accs,foo):
   # print (accs)
    # fetch a protein
    chembl_target_ids = set()
#    foo = open('uniprot2chembl1.csv', 'w')
    foo = open(foo, 'w')
    index = 0
    for acc in accs:
        url = 'https://www.ebi.ac.uk/chembl/api/data/target.json?target_components__accession='+acc
        #url = 'http://gpcrdb.org/services/protein/adrb2_human/'
        #response = urlopen(url) #python 2
        #protein_data = json.loads(response.read()) python 2
        response = requests.get(url)  #python 3
        protein_data = response.json()
#        print (protein_data)
        try:
            if protein_data['targets'][0]['target_type'] == 'SINGLE PROTEIN':#or protein_data['targets'][0]['target_type'] == 'PROTEIN COMPLEX' :
                chembl_id = protein_data['targets'][0]['target_chembl_id']
                foo.write('%s\t%s\t%s\n' %(acc,chembl_id,protein_data['targets'][0]['target_type'] ))
                index = index + 1 
                chembl_target_ids.add(chembl_id)
                #print(chembl_id)
     #       get_chembl_ligads(chembl_id)
        except (IndexError):
            pass
    print (index)
    return chembl_target_ids
    foo.close()
        
    
def read_input_file(filename):
    acc_numbers=[]
    with open(filename, 'r') as fii:
        for line in fii:
            if line != '\n':
            #print line
                temp = line.rstrip().split()
                acc_numbers.append(temp[-1])
    
    return acc_numbers
#            get_chembl_id(temp[-1]


def main(argv):
    input_filename = argv[0]
    output_filename = argv[1]
    uniprot_accs=read_input_file(input_filename)
    #print input_filename
    #uniprot_acc = 'C0M5K6'
    #uniprot_accs = 'P46092'
    #print uniprot_accs
    target_ids = get_chembl_id(uniprot_accs, output_filename)

if __name__ in "__main__":
    main(sys.argv[1:])
    #main()
