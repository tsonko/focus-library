#!/usr/bin/python
import sys
import requests


def main (argv):
    inputFile= argv[0]
    foo = open('slugs', 'w')
#    print inputFile
    with open(inputFile, 'r') as fii:
        for line in fii:
            temp = line.rstrip().lower().split()
            acc = temp[1]
            entry_name = temp[2]
            rev= temp[3]
            url = "http://gpcrdb.org/services/protein/"+entry_name
            #url = "http://test.gpcrdb.org:80/services/protein/accession/" + entry_name
            slug_data = requests.get(url).json()
            try :
                 slug = slug_data['family']
                 url2 = "http://test.gpcrdb.org/services/proteinfamily/"+slug
                 family_data=requests.get(url2).json()
                 #foo.write(family_data)
                 #print '%s\t%s\t%s' %(acc,entry_name,slug)
                 test = slug.split('_')[1]
                 if test == '002':
                     print '%s\t%s\t%s\t%s' %(acc.upper(), family_data['parent']['name'],family_data['name'],'P')
                 else:
                     print '%s\t%s\t%s\t%s' %(acc.upper(), family_data['parent']['name'],family_data['name'],'N')



            except KeyError:
                  foo.write('%s\t%s\t%s\n' %(acc.upper(),entry_name,rev))


if __name__ == "__main__":
    main(sys.argv[1:])
